//  _____ ___ ____   _____  _    ____   _____ ___  _____ ____ ___ _____ 
// |_   _|_ _/ ___| |_   _|/ \  / ___| |_   _/ _ \| ____/ ___|_ _| ____|
//   | |  | | |       | | / _ \| |       | || | | |  _| \___ \| ||  _|  
//   | |  | | |___    | |/ ___ \ |___    | || |_| | |___ ___) | || |___ 
//   |_| |___\____|   |_/_/   \_\____|   |_| \___/|_____|____/___|_____|
//
//  Javascript and logic: Indre Kvedaraite
//  Visual Stuffs, html/css: Victor Evertsson Heijler
//  Version: 0.1
//  Date: 2015-06-11
//

/*
Game vars
*/
var gameBoard = document.getElementById("board");
var buttons = document.getElementById("buttons");
var notification = document.getElementById("game-end");
var you = document.getElementById("you");
var bot = document.getElementById("bot");
var endMessage = document.getElementById("game-end-message");
var haveWinner = false;
var player = "none";
var computer = "none";
var turn = false; // true is player's turn, false is computer's turn
var l, k, u; //vars for loops, so vsc calms down a bit
var gameOver = false;
var turns = 0; // how many turns were made
var playerScore = 0;
var computerScore = 0;

//Which tiles are taken
var takenTiles = [ 
	false, false, false,
	false, false, false,
	false, false, false
];

// Possible winning trplets 
var possibleWins = [
    ["1", "2", "3"], //0
    ["4", "5", "6"], //1
    ["7", "8", "9"], //2
    ["1", "4", "7"], //3
    ["2", "5", "8"], //4
    ["3", "6", "9"], //5
    ["1", "5", "9"], //6
    ["3", "5", "7"] //7
];

/* 
Game functions
*/


// Start new game, runs when clicked "restart" button after game end

function newGame (el) {
	el.classList.add('hide');
    el.classList.remove('show');
	buttons.style.display = 'block';
	removeStuff();
	fixWins();
	emptyTiles();
	player = "none";
	computer = "none";
	turn = false;
	turns = 0;
	gameOver = false;
	notification.style.display= "none";
	you.classList.remove("pulse");
	bot.classList.remove("pulse");
	haveWinner = false;
	backgroundChange();
	niceMessage(true);
}

// Player's and computer's type after choice
function choice (mark) {
	player = mark;
	buttons.style.display = "none";
	if (player == 'x') {
		computer = 'o';
		turn = true;
	}
	else if (player == 'o') {
		computer = 'x';
		turn = false;
		computerMove();
	}
	fadeIn(gameBoard);
}

// Random number generator
function randomGen (min, max) {
	var randomNum = Math.floor(Math.random() * (max - min+1) + min);
	return randomNum;
}

// What computer does when it's its move
function computerMove () {
	var whatTile = randomGen(1, 9);
	var strTile = whatTile.toString();
	if (takenTiles[whatTile - 1] === true && gameOver === false && turns != 9) {
		computerMove();
	}
	else if (takenTiles[whatTile - 1] === false && turn === false && turns != 9 && gameOver === false) {
		if(computer == 'x') { 
			document.getElementById("tile-"+strTile).classList.add('cross');
			for (l = 0; l < 8; l++) {
				for (k = 0; k < 3; k++) {
					if(possibleWins[l][k] == strTile) {
						possibleWins[l][k] =  "x";
					}
				}
			}
			takenTiles[whatTile - 1] = true;
			turn = true;
			turns++;
			whoWon("x");
		}
		else if (computer == 'o') {
				document.getElementById("tile-"+strTile).classList.add('circle');
				for (l = 0; l < 8; l++){
					for (k = 0; k < 3; k++) {
					if (possibleWins[l][k] == strTile) {
						possibleWins[l][k] = "o";
						}
					}
				}
				takenTiles[whatTile - 1] = true;
				turn = true;
				turns++;	
				whoWon("o"); 
		}
	}
	else if (gameOver === true || turns == 9) {
		return;
	}
}


//When player makes a move
function playerMove (el, tileNum) {
	if (player == "x" && takenTiles[tileNum - 1] === false && turn === true && gameOver === false) {
		el.classList.add('cross');
		el.classList.remove('cross-hover');
		turn = false;
		takenTiles[tileNum - 1] = true;
		for (l = 0; l < 8; l++) {
			for (k = 0; k < 3; k++) {
				if(possibleWins[l][k] == tileNum) {
					possibleWins[l][k] =  "x";
				}
			}
		}
		turns++; 
		whoWon("x");
    }
    
    else if (player == "o" && takenTiles[tileNum - 1] === false && turn === true && gameOver === false) {
        el.classList.add('circle');
        el.classList.remove('circle-hover');
		turn = false;
		takenTiles[tileNum - 1] = true;
		for (l = 0; l < 8; l++) {
			for (k = 0; k < 3; k++) {
				if(possibleWins[l][k] == tileNum) {
					possibleWins[l][k] =  "o";
				}
			}
		}
		turns++;
		whoWon("o"); 
	}
	else {
		return;
	}
	computerMove();
}

//This will check who won 
function whoWon (sym) {
	for (u = 0; u <= 7; u++) {
		if (possibleWins[u][0] == sym && possibleWins[u][1] == sym && possibleWins[u][2] == sym) {
			gameOver = true;
			haveWinner = true;
			notification.style.display = "";
				if (sym == "x") {
					endMessage.innerHTML = "X won!";
					differentSym(u, "cross-win");
					if (player == "x") {
						playerScore++;
						you.classList.add("pulse");
						niceMessage();
					}
					else if (computer == "x") {
						computerScore++;
						bot.classList.add("pulse");
					}
				}
				else if (sym == "o") {
					differentSym(u, "circle-win");
					endMessage.innerHTML = "O won!";
					if (player == "o") {
						playerScore++;
						you.classList.add("pulse");
						niceMessage();
					}
					else if (computer == "o" ){
						computerScore++;
						bot.classList.add("pulse");
					}
				}
				
			var strPlayer = playerScore.toString();
			var strComp = computerScore.toString();
			you.innerHTML = strPlayer;
			bot.innerHTML = strComp;

		}
	}
	if (turns == 9 && haveWinner === false && gameOver === false){
		notification.style.display = "";
		gameOver = true;
		endMessage.innerHTML = "It's a tie!";
		return "nothing";
	}
}

// Fix possibleWins array after end game
function fixWins () {
	var zero = ["1", "2", "3"];
	var one = ["4", "5", "6"];
	var two = ["7", "8", "9"];
	var three = ["1", "4", "7"];
	var four = ["2", "5", "8"];
	var five = ["3", "6", "9"];
	var six = ["1", "5", "9"];
	var seven = ["3", "5", "7"];
	possibleWins = [];
	possibleWins.push(zero);
	possibleWins.push(one);
	possibleWins.push(two);
	possibleWins.push(three);
	possibleWins.push(four);
	possibleWins.push(five);
	possibleWins.push(six);
	possibleWins.push(seven);
}

// Will empty every single spot in tilesTaken
function emptyTiles () {
	takenTiles = [];
	for (l = 0; l < 9; l++) {
		takenTiles[l] = false;
	}
}

/*

Visual stuff

*/
// Decides where to change symbols after win
function differentSym (lett, name) {
	switch (lett) {
		case 0:
			changeSym(1, 2, 3, name);
			break;
		case 1:
			changeSym(4, 5, 6, name);
			break;
		case 2:
			changeSym(7, 8, 9, name);
			break;
		case 3:
			changeSym(1, 4, 7, name);
			break;
		case 4:
			changeSym(2, 5, 8, name);
			break;
		case 5:
			changeSym(3, 6, 9, name);
			break;
		case 6:
			changeSym(1, 5, 9, name);
			break;
		case 7: 
			changeSym(3, 5, 7, name);
			break;
		default:
			console.log("something's not good in switch");
	}
}


// Changes symbol color when won
function changeSym (num1, num2, num3, name) {
	var num1Str = num1.toString();
	var num2Str = num2.toString();
	var num3Str = num3.toString();
	var one = document.getElementById("tile-"+num1Str).classList.add(name, "flip");
	var two = document.getElementById("tile-"+num2Str).classList.add(name, "flip");
	var three = document.getElementById("tile-"+num3Str).classList.add(name, "flip");
	
	
}

//Fades in game board
function fadeIn (el) {
	el.classList.add('show');
    el.classList.remove('hide');	
}

//Will remove x or o when game is restarted
function removeStuff () {
	var l;
	var tile;
	for (l = 1; l < 10; l++) {
		var str = l.toString();
		tile = document.getElementById("tile-"+str).classList.remove("cross", "circle", "cross-win", "circle-win", "flip");
	}
	return tile;
	
}

// Paints a thing when hovered
function hovered (i,num) {
	
	if (player == 'x' && gameOver === false) { //player is x
		if (num == '1') {
			if (i.classList.contains('cross') || i.classList.contains('circle')) {
                i.classList.remove('cross-hover');
            }
            else {
                
                i.classList.add('cross-hover');
            }
		}
		else if (num == '2') {
			i.classList.remove('cross-hover');
		}
	}
    
	else if (player == 'o' && gameOver === false) { //player is o
		if (num == '1') {
			if (i.classList.contains('circle') || i.classList.contains('cross')) {
                i.classList.remove('circle-hover');
            }
            
            else {  
                i.classList.add('circle-hover');
            }
		}
		else if (num == '2') {
			i.classList.remove('circle-hover');
		}
	}
	
	else if (gameOver === true) {
		return;
	}
}

// Random background color on new game
function backgroundChange () {
	var color = randomGen(0, 360);
	var strColor = color.toString();
	var background = document.documentElement.style.backgroundColor =  "hsl("+strColor+",65%, 58%)";
	var button = document.getElementById("newGameButton");
	button.style.backgroundColor = "hsl("+strColor+",65%, 58%)";
	return background, button;
}

//Random messages after player win
function niceMessage(reset) {
	var messageList = ["Nice move!","I didn't think of that","Ooh!","That was nice!","Yay","Genius!","Well played!", "I couldn't have made a better move", "If I could I would knight you for that move"];
	var defaultMessage = "Welcome to Tic Tac Toesie";
	var randomMessage = messageList[randomGen(0, messageList.length -1)];
	var message;
	if (reset) {
		message = defaultMessage;
	} else {
		message = randomMessage;
	}
	document.getElementById("chatBubble").innerHTML = "<p class='fade-in'>"+ message +"</p>";
	return;
}